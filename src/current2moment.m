function u = current2moment(current, param)
%
% Calculate magnetic moment from given current
%
% u is magnetic moment
% param is catheter's parameters
%

NA = param.actuator.turnsArea;
u = NA * current;
end