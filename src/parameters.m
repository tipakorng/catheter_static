%
%
% Manually defined parameters
% Run this file first
%
%

%
% Some initialization
%

% First clear param from the work space
clear param;
% Then add utility function's path
addpath('utils', 'utils/robotic_manipulation');

%
% Catheter
%

% Number of joints
num_joints = 4;
% Link lengths (mm)
link_lengths = [40, 30, 30, 5]';
% Radius (mm)
outer_radius = 0.125 * 25.4 / 2;
inner_radius = 0.078 * 25.4 / 2;
% Density (kg / m^3)
catheter_density = 1119.6;
% Young's modulus (MPa)
youngs_modulus = 10;
% Poisson ratio
poissons_ratio = 0.47;

%
% Actuation
%

% Actuator position (mm)
actuator_pos = 100 - 6.25;
% Link containing actuator
actuator_link = 3;
% Actuation upper bound (A)
current_max = [inf, inf, inf]';
% Actuation lower bound (A)
current_min = [-inf, -inf, -inf]';
% Coils' winding number
coil_turns = [30, 30, 130]';
% Coil radius (mm)
wire_radius = 0.5 * 0.0635;
% Coils' dimension (mm)
coil1_width = 2.63;
coil1_length = 18.25;
coil2_width = 2.63;
coil2_length = 18.25;
coil3_radius = 0.5 * 4.45;
% Coil density (kg / m^3)
coil_density = 8.940e3;

%
% Environment
%

% External force position (mm)
disturbance_pos = 100;
% Link containing external force
disturbance_link = 4;
% Gravity in the spatial frame
gravity = 9.81 * [0, 0, 1]';
% Blood density (kg / m^3)
blood_density = 1060;

%
% Surface
%

% Surface origin with respect to base of the catheter (mm)
surface_origin = [0, 0, 80]';
% Orientation of surface coordinate frame with respect to the catheter
% (SO(3))
surface_orientation = eye(3);
% Surface normal with respect to base of the catheter
surface_normal = surface_orientation(:, 3);

%
% Magnetic field
%

% Magnetic field (T)
magnetic_field = [0, 0, 3]';

%
% Sampling time
%

% Planning sampling time (s)
ts_plan = 1;
% Simulation sampling time (s)
ts_sim = 1;

%
% Generate parameter variable
%
run generate_param.m