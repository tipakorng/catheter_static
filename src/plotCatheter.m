function plotCatheter(q, param)
%
% plotCatheter(q,param)
%
% Plot the trajectory of the catheter from the trajectory of joint angles
%
% q is joint angle vector
% dim is x, y and z dimension of the plot
% param is the struct variable containing parameters of the catheter
%
% Require: tipPosition, spatial2surf

% % Initialize tip position
% x = zeros(3, size(q, 2));
% Initialize figure
figure; hold on; grid on;
len = sum(param.catheter.linkLength);
axis([-len, len, -len, len, 0, len]);
xlabel('x (mm)');
ylabel('y (mm)');
zlabel('z (mm)');
% Plot catheter for each joint angle vector
for itr = 1:1:size(q, 2)
%     x(1:3, itr) = tipPosition(q(:, itr),param);
    % Plot catheter body
    % Draw the catheter
    p = jointLocation(q(:, itr),param);
    plot3(p(1, :),p(2, :),p(3, :), 'Color', 'b', 'LineWidth', 1);
end
% Plot tip trajectory
% plot3(x(1, :), x(2, :), x(3, :), 'Color', 'r', 'LineWidth', 1);
hold off;
end