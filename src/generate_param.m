%
%
% Generate parameter variable
%
%

%
% Catheter
%

% Area moment of inertia (mm^4)
moment_of_inertia = 0.25 * pi * (outer_radius^4 - inner_radius^4);
% Joint spring stiffnesses (N mm)
stiffnesses = [youngs_modulus * moment_of_inertia / link_lengths(1), ...
    youngs_modulus * moment_of_inertia / link_lengths(2), ...
    youngs_modulus * moment_of_inertia / link_lengths(3), ...
    youngs_modulus * moment_of_inertia / link_lengths(4)]';
% Joint direction
joint_dir = repmat([1, 0, 0; 0, 1, 0]', 1, num_joints);
% Joint position (mm)
init_joint_pos = zeros(3, num_joints);
for n = 1 : 1 : num_joints
    init_joint_pos(3, n+1) = sum(link_lengths(1:n));
end
% Spring stiffenss matrix (N mm / Rad)
stiffness_matrix = diag(kron(reshape(stiffnesses, 1, length(stiffnesses)), [1, 1]));
% Damping matrix (N mm / Rad s)
damping_matrix = [];
% Mass (kg)
link_masses = pi * (outer_radius^2 - inner_radius^2) * ...
    (catheter_density - blood_density) * 1e-9 * link_lengths';
% Create struct variable
catheter = struct('jointNumber', num_joints, ...
    'jointDir', joint_dir, ...
    'jointPosition', init_joint_pos, ...
    'linkLength', link_lengths, ...
    'jointSpringCoeff', stiffness_matrix, ...
    'fluidDampingCoeff', damping_matrix, ...
    'linkMass', link_masses);

%
% Actuator
%

% Coils' area (mm^2)
coil_area = [(coil1_width - 2 * wire_radius) * (coil1_length - 2 * wire_radius), ...
    (coil2_width - 2 * wire_radius) * (coil2_length - 2 * wire_radius), ...
    pi * (coil3_radius - wire_radius)^2]';
% Turns area matrix (N mm / T A (see notes on 3/30/2015))
NA = 1e-3 * diag([coil_turns(1)*coil_area(1), ...
    coil_turns(2)*coil_area(2), ...
    coil_turns(3)*coil_area(3)]);
% Calculate actuator initial configuration
g_sa0 = [eye(3), [0, 0, actuator_pos]'; 0, 0, 0, 1];
% Calculate coil masses
coil_masses = pi * wire_radius^2 * (coil_density - blood_density) * ...
    ((coil1_width + coil1_length) * 2 * coil_turns(1) + ...
    (coil2_width + coil2_length) * 2 * coil_turns(2) + ...
    2 * pi * (inner_radius + coil3_radius) * coil_turns(3)) * 1e-9;
% Create actuation struct parameter
actuator = struct('initConf', g_sa0, ...
    'link', actuator_link, ...
    'ctrlMax', current_max, ...
    'ctrlMin', current_min, ...
    'turnsArea', NA, ...
    'coilMass', coil_masses);

%
% Environment
%

% Disturbance initial conf
g_sd0 = [eye(3), [0, 0, disturbance_pos]'; 0, 0, 0, 1];
% Links' CM initial conf
g_sl = zeros(4, 4, num_joints);
for n = 1:1:num_joints
    g_sl(1:4, 1:4, n) = [eye(3), [0; 0; sum(link_lengths(1:n)) - 0.5 * link_lengths(n)];
                         0, 0, 0, 1];
end
% Create disturbances struct parameter
environment = struct('distbInitConf', g_sd0,...
                       'distbLink', disturbance_link,...
                       'linkCMConf', g_sl, ...
                       'gravity', gravity);

%
% Surface
%

% Surface origin initial configuration
g_sc0 = [surface_orientation, surface_origin; 0, 0, 0, 1];
% Create surface struct variable
surface = struct('initConf', g_sc0, ...
    'origin', surface_origin, ...
    'orientation', surface_orientation, ...
    'normal', surface_normal);

%
% Magnetic field
%

% Create magnetic field struct variable
magneticField = struct('dir', magnetic_field);

%
% Sampling time
%

% Create struct
T = struct('plan', ts_plan, 'simulation', ts_sim);
% Combine all parameters into one variable
param = struct('catheter', catheter, ...
    'surface', surface, ...
    'actuator', actuator, ...
    'environment', environment, ...
    'magneticField', magneticField, ...
    'samplingTime', T);
