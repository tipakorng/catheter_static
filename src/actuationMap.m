function [g_su,J_u,B,P] = actuationMap(q,param)
%
% Calculates conf of actuation frame, actuator Jacobian, compact form of 
% magnetic field matrix, and input current projection matrix
%
% g_su is the configuration of the actuator with respect to the base frame
% J_u is the bottom half of the actuator Jacobian
% B is the MRI's magnetic field matrix without the null space
% P is the magnetic moment in R^3 to the input in R^2
%
% Require: se3rot, adjinv, skewsym2vector
%

% Parameters
q_0 = param.catheter.jointPosition; % Joint position
w = param.catheter.jointDir;        % Joint direction
B_s = param.magneticField.dir;      % Magnetic field in the spatial frame
% Init
g_su = param.actuator.initConf;     % Initial conf
J = zeros(6,2*param.catheter.jointNumber); % Init body Jacobian
% Calculate body Jacobian
for i = param.actuator.link:-1:1
    g = se3rot(w(:,2*i-1)*q(2*i-1)+w(:,2*i)*q(2*i),q_0(:,i),1);
    Dg_1 = (g - se3rot(w(:,2*i-1)*(q(2*i-1)-1e-8)+w(:,2*i)*q(2*i),q_0(:,i),1))/1e-8;
    Dg_2 = (g - se3rot(w(:,2*i-1)*q(2*i-1)+w(:,2*i)*(q(2*i)-1e-8),q_0(:,i),1))/1e-8;
    [Dg1, Dg2] = velocityAxes(q(2*i-1), q(2*i));
    xi_1 = Dg_1/g;
    xi_2 = Dg_2/g;
    g_su = g*g_su;
    J(:,2*i-1) =  adjinv(g_su)*[xi_1(1:3,4);skewsym2vector(xi_1(1:3,1:3))];
    J(:,2*i) =  adjinv(g_su)*[xi_2(1:3,4);skewsym2vector(xi_2(1:3,1:3))];
end
% Calculate compact form of magnetic field cross product
B_b = g_su(1:3,1:3)'*B_s;           % Magnetic field in the body frame
[U,S,V] = svd(-vector2skewsym(B_b)); % SVD of -B_b (minus sign from reversing order of cross product)
B = U(:,1:2)*S(1:2,1:2);            % Calculate the compact representation of B_b
P = V(:,1:2)';                      % Input projection
% Actuator Jacobian
J_u = J(4:6,:);                     % Bottom part of the Jacobian

end