function J = tipJacobian(q, u, param)

%
% Calculate the Jacobian of tip position with respect to input current.
%   J = dp/du = dp/dq * dq/du
% where p is tip position,
% q is joint angles,
% and u is the input magnetic moment.
% Finite difference is used to calculate the partial derivatives.
% The Jacobian dq/du is calculated according to implicit function theorem.
%
% J is the Jacobian dp/du
% q is the joint angles
% u is the actuator magnetic moment
% param is the parameters of the catheter
%

% Step size
eps = 10e-8;
% Jacobian Dp = [dp/dq] using forward difference
Dp = myJacobian(@(q)tipPosition(q, param), q, eps);
% Jacabian Dq = [dq/du] using forward finite different and implicit function theorem
K = param.catheter.jointSpringCoeff;
fun = @(u, q)(K * q - actuationTorque(q, u, param));
Dq = -myJacobian(@(q)fun(u, q), q, eps) \ myJacobian(@(u)fun(u, q), u, eps);
% Total Jacobian
J = Dp * Dq;

end