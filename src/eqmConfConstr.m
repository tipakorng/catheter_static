function q = eqmConfConstr(q_0,x,u,w,param)

%
% Find the equilibrium configuration given initial guess, tip position of the surface,
% actuation, external force, and parameters
%
% q is the joint angles
% q_0 is the initial guess
% x is the position on the surface
% u is the actuator magnetic moment
% w is the external force
% param is catheter's parameters
%

    % Parameters
    K = param.catheter.jointSpringCoeff;
    % Set the initial guess
    q = q_0;
    % DOF
    n = size(q,1);
    % Initialize termination criterion
    error = 1;
    % Number of iteration
    i = 0;
    % Set number of max iteration
    itr_max = 500;
    % Set tolerance for the gradient of the Lagrangian
    eps = 1e-8;
    % Generate the required QP parameters
    % Initialize BFGS matrix
    B = eye(length(q_0));

    while error>=eps && i<itr_max
        % Store x from previous iteration
        q_old = q;
        % Update number of iteration
        i = i+1;
        
        % Generate the required QP parameters for the next iteration
        % Tip constraints
        h = tipPositionConstr(q,x,param);
        % Jacobian of tip constraints
        Dh = myJacobian(@(q)tipPositionConstr(q,x,param),q,1e-8);
        % Actuator torques
        T = actuatorJointTorque(q,u,param);
        % Disturbance torques
        T_d = distbJointTorque(q,w,param);
        % Gradient of objective function
        gradf = K*q - T - T_d;
        
        % Solve the local QP
        % Set options for QP
        options = optimset('Algorithm','active-set','Display','off');
        % Run QP
        [pk,~,~,~,lk] = quadprog(B,gradf,[],[],Dh,-h,-pi/2*ones(n,1)-q,pi/2*ones(n,1)-q,zeros(n,1),options);
        % Update current points in SQP
        q = q + 0.5*pk;                 % Line search would be nice here
        % Update Lagrange multipliers
        lambda = lk.eqlin;
        
        % BFGS update
        % Store Gradient of Lagrangian function from previous iteration
        DL = @(q)(K*q + myJacobian(@(q)tipPositionConstr(q,x,param),q,1e-8)'*lambda - actuatorJointTorque(q,u,param) - distbJointTorque(q,w,param));
        % Perform damped BFGS update
        B = dampedBFGS(B,q,q_old,DL);
    %     % Persorm SR1 update
    %     B = SR1(B,x,x_old,DL);
        % Force symmetry
        B = (B+B')/2;
        % Check Jacobian of Lagrangian function for termination
        % error = norm(DL(q),2);
        error = norm(pk,2);
    end
end

%%
function h = tipPositionConstr(q,x_d,param)
%
% Tip position constraint for fixed tip position
%
    % Tip position error
    h = tipPosition(q,param) - x_d;
end