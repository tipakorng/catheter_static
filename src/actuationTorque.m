function T = actuationTorque(q,u,param)
%
% Calculate joint torque vector for a given configuration and current.
% The current can be 3-dim or 2-dim projection.
%
% T is the actuator torque
% q is the joint angles
% u is the magnetic moment
% param is the catheter's parameters
%
% Require: actuationMap
%

% Get actuation matrices
[g_su,J_u,B,~] = actuationMap(q,param);
% Calculate joint torques
if length(u) == 2
    T = J_u'*B*u;               % Calculate joint torques
else
    b = g_su(1:3,1:3)'*param.magneticField.dir; % Magnetic field in the body frame
    T = J_u'*cross(u,b);        % Calculate joint torques
end
end