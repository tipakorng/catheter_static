function current = moment2current(u, param)
%
% Calculate current for a given magnetic moment
%
% u is magnetic moment
% param is catheter's parameters
%

NA = param.actuator.turnsArea;
current = NA \ u;
end