function J = tipInvJacobian(q, u, param)
%
% Calculate J = du/dp where du is a small increment in actuation magnetic
% moment and dp is a small increment in tip position.
%

% Calculate Jacobian Jpu = dp/du
J = tipJacobian(q, u, param);

% SVD of J
[U, S, V] = svd(J);
% Truncated SVD
U0 = U(:, 1:2);
S0 = S(1:2, 1:2);
V0 = V(:, 1:2);
% Pseudo inverse of Jpu restricted to the tangent space of Jpu
J = V0 * inv(V0'*V0) * inv(S0) * U0';

end