function B = dampedBFGS(B_0,x_new,x,DL)
%
% Perform BFGS usdate
%

s = x_new - x;
y = DL(x_new) - DL(x);

if s'*y >= 0.2*s'*B_0*y
    t = 1;
else
    t = 0.8*(s'*B_0*s)/(s'*B_0*s - s'*y);
end
r = t*y + (1-t)*B_0*s;

B = B_0 - ((B_0*s)*(s'*B_0))/(s'*B_0*s) + (r*r')/(s'*r);