%
% Calculate spatial Jacobian from w, q, theta. w and q must be matrices
% whose columns are the the column vectors of the rotation axes and the
% joint locations. theta is the joint angles in column vector form.
%
function J = sJacobian(w,q,theta)
n = length(theta);
J = [[cross(q(1:3,1),w(1:3,1)); w(1:3,1)] zeros(6,n-1)];
for i=2:n
    J(1:6,i) = adj(