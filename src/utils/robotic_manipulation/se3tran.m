% Map se(3) to SE(3) in case of pure translation
% From eq 2.41 MLS
function g = se3tran(v,t)
if size(v,2)>1
    disp('error: use row vector');
    return
elseif size(v,1)~=3
    disp('error: vector has length not equal to 3');
    return
else
    % Find g using eq 2.41
    g = [eye(3) v*t; 0 0 0 1];
end