% Find the inverse of Ad_g from g using the equation following eq 2.58
function Ad_gInv = adjinv(g)
R = g(1:3,1:3);
p = vector2skewsym(g(1:3,4));
Ad_gInv = [R' -R'*p; zeros(3) R'];