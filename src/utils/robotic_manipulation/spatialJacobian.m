%
% Calculate spatial Jacobian from w, q, theta. w and q must be matrices
% whose columns are the the column vectors of the rotation axes and the
% joint locations. theta is the joint angles in column vector form.
%
function J = sJacobian(w,q,theta)
twist = [cross(q,w,1);w];