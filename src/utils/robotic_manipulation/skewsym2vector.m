function v = skewsym2vector(M)
v = [M(3,2) M(1,3) M(2,1)]';