% Find Ad_g from g using eq 2.58
function Ad_g = adj(g)
R = g(1:3,1:3);
p = vector2skewsym(g(1:3,4));
Ad_g = [R p*R; zeros(3) R];