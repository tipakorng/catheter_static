function DDf = myHessian(f,x,h)
f_0 = f(x);
n = length(x);
DDf = zeros(n,n);
H_1 = h(1)*eye(n);
H_2 = h(2)*eye(n);
for i = 1:n
    for j = 1:n
        DDf(i,j) = f(x+H_1(:,i)+H_2(:,j)) - f(x+H_1(:,i)) - f(x+H_1(:,j)) + f_0;
    end
end
DDf = DDf/h(1)/h(2);