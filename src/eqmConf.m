function [q, error, itr] = eqmConf(q_0, u, w, param)

%
% Find the equilibrium configuration given initial guess, actuation,
% external force, and parameters
%
% q is the joint angles
% error is the norm of the local QP's solution
% itr is number of iteration
% q_0 is the initial guess
% u is the actuator magnetic moment
% w is the external force
% param is catheter's parameters
%

    % Parameters
    K = param.catheter.jointSpringCoeff;
    % Set the initial guess
    q = q_0;
    % DOF
    n = size(q, 1);
    % Initialize termination criterion
    error = 1;
    % Number of iteration
    itr = 0;
    % Set number of max iteration
    itr_max = 1000;
    % Set tolerance for the gradient of the Lagrangian
    eps = 1e-8;
    % Generate the required QP parameters
    % Initialize BFGS matrix
    B = K;
    % Step size
    ss = 1e0;

    while error >= eps && itr < itr_max
        % Store x from previous iteration
        q_old = q;
        % Update number of iteration
        itr = itr + 1;
        
        %
        % Generate the required QP parameters for the next iteration
        %
        
        % Actuator torques
        T = actuatorJointTorque(q, u, param);
        % Disturbance torques
        T_d = externalJointTorque(q, w, param);
        % Gradient of objective function
        gradf = K * q - T - T_d;
        
        %
        % Solve the local QP
        %
        
        % Set options for QP
        options = optimset('Algorithm', 'active-set', 'Display', 'off');
        % Run QP
        pk = quadprog(B, gradf, [], [], [], [], -0.5 * pi * ones(n, 1) - q, 0.5 *pi * ones(n, 1) - q, zeros(n, 1), options);
        % Update current points in SQP
        q = q + ss * pk;                 % Line search would be nice here
        
        %
        % BFGS update
        %
        
        % Store Gradient of Lagrangian function from previous iteration
        DL = @(q)(K * q - actuatorJointTorque(q, u, param) - distbJointTorque(q, w, param));
        % Perform damped BFGS update
        B = dampedBFGS(B, q, q_old, DL);
        % Force symmetry
        B = 0.5 * (B + B');
        % Check Jacobian of Lagrangian function for termination
        % error = norm(DL(q),2);
        error = norm(pk, 2);
    end
end
