function h = tipPositionConstr(q,param)
%
% Calculate tip position constraint h(q) = (p(q) - p_0)' * n_0
%
% h is tip position constraint error. If the tip is on the surface, h = 0
% q is joint angles
% param is catheter's parameters
%
% Require: tipPosition(q,param)
%
    p_0 = param.surface.origin;
    n_0 = param.surface.normal;
    h = (tipPosition(q,param) - p_0)' * n_0;
end