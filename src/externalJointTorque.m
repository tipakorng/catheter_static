function [T, J] = externalJointTorque(q, v, param)
%
% Calculate joint torques due to gravity
% T = T = externalJointTorque(q, param)
%
% T is joint torque (column) vector
% q is joint angle (column) vector
% v is disturbance force in R^3
% param is the structure variable containing catheter's parameters
%

%
% Jacobians calculation
%

% Joint position
q0 = param.catheter.jointPosition;
% Joint direction
w = param.catheter.jointDir;
% Initial gravity vector in spatial frame
g = param.environment.gravity;
% Link masses
mass = param.catheter.linkMass;
% Initialize joint torque vector
T = zeros(2*param.catheter.jointNumber, 1);
% Initialize body Jacobian
J = zeros(6, 2*param.catheter.jointNumber, param.catheter.jointNumber);
% Calculate body Jacobians
for linkNumber = 1:1:param.catheter.jointNumber
    % Initial conf of link's CM
    g_sl = param.environment.linkCMConf(:, :, linkNumber);
    % Calculate Jacobian
    for n = linkNumber:-1:1
        g_sl = se3rot(w(:,2*n-1)*q(2*n-1)+w(:,2*n)*q(2*n),q0(:,n),1)*g_sl;
        [w1, w2] = velocityAxes(q(2*n-1), q(2*n));
        xi1 = twistr(w1, q0(:, n));
        xi2 = twistr(w2, q0(:, n));
        J(:, 2*n-1, linkNumber) =  adjinv(g_sl)*xi1;
        J(:, 2*n, linkNumber) =  adjinv(g_sl)*xi2;
    end
    % Gravity in body frame of the current link
    g_b = g_sl(1:3, 1:3)' * g;
    % Add to current joint torques
    T = T + J(1:3, :, linkNumber)' * mass(linkNumber) * g_b;
    % If disturbance is acting on this link, add corresponding joint torque
    if linkNumber == param.environment.distbLink
        T = T + J(1:3, :, linkNumber)' * v;
    end
end

end