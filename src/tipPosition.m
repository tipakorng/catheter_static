function x = tipPosition(q,param)
%
% x = tipPosition(q,param)
% Calculate tip position, x, from given joint angles, q.
%
% x is the catheter's tip position
% q is the joint angles
% param is catheter's parameters
%

% Link dimension
l = param.catheter.linkLength;
% Joint position
q_0 = param.catheter.jointPosition;
% Joint direction
w = param.catheter.jointDir;
% Init exp matrix
g_st = [eye(3),[0;0;sum(l)]; 0 0 0 1];

for itr = param.catheter.jointNumber:-1:1
    g_st = se3rot(w(:,2*itr-1)*q(2*itr-1)+w(:,2*itr)*q(2*itr),q_0(:,itr),1)*g_st;
end

% Tip position
x = g_st(1:3,4);