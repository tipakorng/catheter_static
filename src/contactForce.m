function [f,J] = contactForce(q,u,d,param)
%
% [f,J] = contactForce(q,u,w,param)
%
% Calculate contact force and hand Jacobian
%
% f is the contact force
% J is the Hand Jacobian (see eqn 5.14 MLS)
% q is the joint angles
% u is the actuator magnetic moment
% d is the external force
% param is the catheter's parameters
%
% Require: contactFrame, actuatorJointTorque, distbJointTorque
%

% Parameters
K = param.catheter.jointSpringCoeff;
g_sc = contactFrame(q,param);
g_sf = eye(4);
w = param.catheter.jointDir;
q_0 = param.catheter.jointPosition;
% Wrench basis
B_c = [1 0 0; 0 1 0; 0 0 1; zeros(3)];
% Jacobian from base to tip frame
J_sf = zeros(6,2*param.catheter.jointNumber);
for i = 1:1:param.catheter.jointNumber
    g = se3rot(w(:,2*i-1)*q(2*i-1)+w(:,2*i)*q(2*i),q_0(:,i),1);
    Dg_1 = (g - se3rot(w(:,2*i-1)*(q(2*i-1)-1e-8)+w(:,2*i)*q(2*i),q_0(:,i),1))/1e-8;
    Dg_2 = (g - se3rot(w(:,2*i-1)*q(2*i-1)+w(:,2*i)*(q(2*i)-1e-8),q_0(:,i),1))/1e-8;
    xi_1 = Dg_1/g;
    xi_2 = Dg_2/g;
    J_sf(:,2*i-1) = adj(g_sf)*[xi_1(1:3,4);skewsym2vector(xi_1(1:3,1:3))];
    J_sf(:,2*i) = adj(g_sf)*[xi_2(1:3,4);skewsym2vector(xi_2(1:3,1:3))];
    g_sf = g_sf*se3rot(w(:,2*i-1)*q(2*i-1)+w(:,2*i)*q(2*i),q_0(:,i),1);
end
% Hand Jacobian eq5.14 MLS
J = B_c'*adjinv(g_sc)*J_sf;

% Torque from actuator
T = actuatorJointTorque(q,u,param);
% Torque from disturbance
T_d = distbJointTorque(q,d,param);
% Contact force
f = (J*J')\J*(-K*q+T+T_d);
end