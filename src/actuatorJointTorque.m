function [T,B,J,B_b] = actuatorJointTorque(q,u,param)
%
% [T,B,J,B_b] = actuatorJointTorque(q,u,param)
% Calculate joint torques for a given configuration and control
%
% T is the joint torques
% B is input mapping
% J is the manipulator Jacobian of the actuator
% B_b is the magnetic field in the body frame
% q is the joint angles
% u is the actuator
% param is the catheter's parameters
%

% Magnetic field in the spatial frame
B_s = param.magneticField.dir;
% Joint position
q_0 = param.catheter.jointPosition;
% Joint direction
w = param.catheter.jointDir;
% Initial gravity vector in spatial frame
g = param.environment.gravity;
% Initial conf
g_sa = param.actuator.initConf;
% Init body Jacobian
J = zeros(6,2*param.catheter.jointNumber);
% Calculate body Jacobian
for n = param.actuator.link:-1:1
    g_sa = se3rot(w(:,2*n-1)*q(2*n-1)+w(:,2*n)*q(2*n),q_0(:,n),1) * g_sa;
    [w1, w2] = velocityAxes(q(2*n-1), q(2*n));
    xi1 = twistr(w1, q_0(:, n));
    xi2 = twistr(w2, q_0(:, n));
    J(:, 2*n-1) = adjinv(g_sa) * xi1;
    J(:, 2*n) = adjinv(g_sa) * xi2;
end
% Magnetic field in the body frame
B_b = g_sa(1:3,1:3)'*B_s;
% Gravity in the body frame
g_b = g_sa(1:3, 1:3)' * g;
% Joint torques
T = J'*[param.actuator.coilMass * g_b; cross(u, B_b)];
% 'B' matrix
B = -J(4:6,:)'*vector2skewsym(B_b);

end