%% Parameters
addpath('utils', 'utils/robotic_manipulation');
clear param;

%
% Catheter parameters
%

% Number of joints
n = 3;
% Joint direction
w = [1 0 0; 0 1 0; 1 0 0; 0 1 0 ; 1 0 0; 0 1 0]';
% Link lengths
l = [40 30 30]'*1e-3;
% Joint initial location
q_0 = [0 0 0; 0 0 l(1); 0 0 l(1)+l(2); 0 0 l(1)+l(2)+l(3)]';
% Joint springs
K = 5e-4*diag([1,1,1,1,1,1]);
% Fluid damping coefficient (see notes on 4/2/13 + some recalculation)
C_d = 2.5e-4*diag([2,2,2,2,0.5,0.5]);
% Friction coefficient
mu = 0.0;
% Tip max speed
v_max = 1e-3;
% Combine catheter parameters
catheter = struct('jointNumber',n,'jointDir',w,'jointPosition',q_0,'linkLength',l,...
    'jointSpringCoeff',K,'fluidDampingCoeff',C_d,'frictionCoeff',mu,...
    'tipSpeed',v_max);

%
% Actuation
%

% Actuator initial pose
g_sa0 = [eye(3) [0 0 40]'*1e-3;0 0 0 1];
% Link containing the actuator
l_a = 2;
% Control upper limit (see notes on 4/2/13)
u_max = inf*ones(3,1);
% Control lower limit (see notes on 4/2/13)
u_min = -inf*ones(3,1);
% Number of turns times area
NA = diag([1, 1, 1]);
% Create actuation struct parameter
actuator = struct('initConf',g_sa0,'link',l_a,'ctrlMax',u_max,'ctrlMin',u_min,...
    'areaTurns', NA);

%
% Disturbances
%

% Disturbance initial conf
g_sd0 = [eye(3) [0 0 30]'*1e-3;0 0 0 1];
% Link containing the disturbance
l_d = 2;
% Create disturbances struct parameter
disturbance = struct('initConf',g_sd0,'link',l_d);

%
% Surface parameters
%

% The configuration of the origin of the surface
g_sc0 = se3rot([0 1 0]',[0 0 0]',0)*[eye(3),[0 0 35]'*1e-3;0 0 0 1];
% A origin of the surface in the base frame
p_0 = g_sc0(1:3,4);
% Surface normal in the base frame
n_0 = g_sc0(1:3,1:3)*[0 0 1]';
surface = struct('initConf',g_sc0,'origin',p_0,'normal',n_0);

%
% Magnetic field parameters
%

% Magnetic field
B = [0 0 1.5]';
% B = [0 1.5 0]';
magneticField = struct('dir',B);

%
% Sampling time
%

% Planning sampling-time
T_p = 0.1;
% Simulation sampling-time
T_s = 0.1;
% Create struct
T = struct('plan',T_p,'simulation',T_s);

% Combine all parameters into one variable
param = struct('catheter',catheter,'surface',surface,...
    'actuator',actuator,'disturbance',disturbance,...
    'magneticField',magneticField,...
    'samplingTime',T);
