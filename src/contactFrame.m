function g_sc = contactFrame(q,param)
%
% Calculate the configuration of the contact frame g_sc with respect to 
% the base frame. Assume the catheter tip is in contact with the surface.
%
% g_sc is the contact frame
% q is the joint angles
% param is the catheter's parameters
%
% Require: tipPosition()
%

% Orientation
R = param.surface.initConf(1:3,1:3); % assume flat surface
% Tip position
x = tipPosition(q,param);
% Calculate g_sc
g_sc = [R x; 0 0 0 1];
end