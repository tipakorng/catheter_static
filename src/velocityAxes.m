function [wx, wy] = velocityAxes(qx, qy)
%
% Calculate velocity axes, wx and wy, of the catheter given its joint
% angles. See [1] for more details.
% [1] T Greigarn and MC Cavusoglu, "Pseudo-Rigid-Body Model and Kinematic Analysis
% of MRI-Actuated Catheters", ICRA 2015
%
% [wx, wy] = velocityAxes(qx, qy, param)
%
% wx is velocity axis in the x-direction
% wy is velocity axis in the y-direction
% qx is joint angle in the x-direction
% qy is joint angle in the y-direction
%

% Total rotation
q = sqrt(qx^2 + qy^2);

% Calculating wx
if abs(qy) < 1e-8
    wx = [1, 0, 0]';
else
    wx = [1 - qy^2 / q^2 * (1 - sin(q) / q); ...
          qx * qy / q^2 * (1 - sin(q) / q); ...
          -qy / q^2 * (1 - cos(q))];
end

% Calculating wy
if abs(qx) < 1e-8
    wy = [0, 1, 0]';
else
    wy = [qx * qy / q^2 * (1 - sin(q) / q); ...
          1 - qx^2 / q^2 * (1 - sin(q) / q); ...
          qx / q^2 * (1 - cos(q))];
end

end