function p = jointLocation(q, param)
%
% jointLocation(q, param)
%
% Returns locations of all the joints in the spatial frame
%

% Number of joints
n = param.catheter.jointNumber;
% Joint directions
w = param.catheter.jointDir;
% Initial joint position
p_0 = param.catheter.jointPosition;
% Initialize joint position and configuration
p = zeros(4,n+1);
g = eye(4);
% Calculate joint locations
for i = 1:1:n
    p(:,i) = g*[p_0(:,i);1];
    g = g*se3rot(w(:,2*i-1)*q(2*i-1)+w(:,2*i)*q(2*i),p_0(:,i),1);
end
p(:,n+1) = g*[p_0(:,n+1);1];

end