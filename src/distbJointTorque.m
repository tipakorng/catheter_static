function [T,B,J] = distbJointTorque(q,v,param)
%
% Calculate joint torques for a given configuration and external force
% assuming the external force acts on a single point of the catheter.
%
% T is the joint torques from external force
% B maps external forces to joint torques
% J is the Jacobian at the external force's frame
%

% Joint position
q_0 = param.catheter.jointPosition;
% Joint direction
w = param.catheter.jointDir;
% Initial conf
g_sd = param.environment.distbInitConf;
% Init body Jacobian
J = zeros(6,2*param.catheter.jointNumber);
% Calculate body Jacobian
for i = param.environment.distbLink:-1:1
    g_sd = se3rot(w(:,2*i-1)*q(2*i-1)+w(:,2*i)*q(2*i),q_0(:,i),1)*g_sd;
    [w1, w2] = velocityAxes(q(2*i-1), q(2*i));
    xi1 = twistr(w1, q_0(:, i));
    xi2 = twistr(w2, q_0(:, i));
    J(:,2*i-1) =  adjinv(g_sd)*xi1;
    J(:,2*i) =  adjinv(g_sd)*xi2;
end
% Joint torques
T = J'*[v;zeros(3,1)];
% Disturbance input matrix (i.e., B matrix)
B = J(1:3,:)';
end