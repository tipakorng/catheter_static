% Clear all
clear
% Initialize parameters
run parameters.m
% define three joint angle vectors
q = zeros(2 * param.catheter.jointNumber, 1);
% Disturbance
w = [0, 0, 0]';
% External joint torque
[T, J] = externalJointTorque(q, w, param);
% Display results
disp('Joint torques = ');
disp(T);
disp('Jacobian = ');
disp(J)