% Clear all
clear
% Initialize parameters
run parameters.m
% Actuator current
current = [0, 0, 0]';
% Actuator magnetic moment
u = current2moment(current, param);
% External force
w = [0, 0, 0]';
% Tip position increment direction
dp = [1, 1, 0]';
% Initial joint angles
q = zeros(2 * param.catheter.jointNumber, 1);

% Move the catheter ten times
for k = 1:1:10
    % Jacobian J = du/dp
    J = tipInvJacobian(q, u, param);
    % Corresponding control
    du = J * dp;
    % Update control
    u = u + du;
    % Calculate new configuration
    q = eqmConf(q, u, w, param);
end
% Display tip position after moving
disp('Tip position');
disp(tipPosition(q, param));