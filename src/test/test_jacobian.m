%
% Example code for calculating the Jacobian dp/du
%

% Initialize parameters
run parameters.m
% Init joint angles
q = 1e-8 * ones(2 * param.catheter.jointNumber, 1);
% Init actuator magnetic moment
u = [0, 0, 0]';
% Calculate Jacobian dp/du
J = tipJacobian(q, u, param);
% Current increment
dc = [1e-2, 0, 0]';
% Magnetic moment increment
du = current2moment(dc, param);
% Calculate tip position increment
dp = J * du;
% Show result
disp('Tip position increment');
disp(dp);