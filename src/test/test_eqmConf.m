%
% Example code for finding equilibrium configuration
%

% Clear all
clear
% Initialize parameters
run parameters.m
% Joint spring
K = param.catheter.jointSpringCoeff;
% Actuator current
current = [0.2, 0, 0]';
% Actuator magnetic moment
u = current2moment(current, param);
% External force
w = [0, 0, 0]';
% Create a random initial guess
q_0 = -0.1 + 0.2 * rand(2 * param.catheter.jointNumber, 1);
% Mark start time
t_start = tic;
% Solve a equilibrium configuration
[q, error, itr] = eqmConf(q_0, u, w, param);
% Calculate elapsed time
t_elapsed = toc(t_start);
% Show results
% Calculate internal torque error
joint_torque_error = K * q - actuatorJointTorque(q, u, param) - externalJointTorque(q, w, param);
% Display results
disp('Joint angles');
disp(q);
disp('Error');
disp(error);
disp('Iterations');
disp(itr);
disp('Elapsed time: ');
disp(t_elapsed);
disp('Internal torque difference');
disp(joint_torque_error);