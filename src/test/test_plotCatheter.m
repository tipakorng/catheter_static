% Clear all
clear
% Initialize parameters
run parameters.m
% define three joint angle vectors
q1 = zeros(2 * param.catheter.jointNumber, 1);
q2 = 1e-1 * ones(2 * param.catheter.jointNumber, 1);
q3 = 2e-1 * ones(2 * param.catheter.jointNumber, 1);
% Then list them to getheter
q = [q1, q2, q3];
% Plot catheter
plotCatheter(q, param);