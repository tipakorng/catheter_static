%
% Example code for inverse Jacobian du/dp
%

% Initialize parameters
run parameters.m
% Init joint angles
q = 1e-8 * ones(2 * param.catheter.jointNumber, 1);
% Init actuator magnetic moment
u = [0, 0, 0]';
% Jacobian J = du/dp
J = tipInvJacobian(q, u, param);
% small increment in tip position
dp = [1, 0, 0]';
% Corresponding control
du = J * dp;
% Input current
dc = moment2current(du, param);
% Display result
disp('Current increment');
disp(dc);