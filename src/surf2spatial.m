function x = surf2spatial(xi,param)
%
% Find a point in the spatial frame for a given point on the surface
% coordinate
%
% x is the position on the surface in the spatial coordinates
% xi is the position on the surface in surface coordinates
% param is catheter's parameters
%

% Parameters
R = param.surface.initConf(1:3,1:3);
% Find p
p = R'*param.surface.initConf(1:3,4);
% Find x
x = R*(p+[xi;0]);
end