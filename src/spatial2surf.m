function xi = spatial2surf(x,param)
%
% Find a point in the surface coordinate for a given point on  the surface
% in the spatial frame
%
% xi is the position on the surface in surface coordinates
% x is the position on the surface in the spatial coordinates
% param is catheter's parameters
%

% Parameters
R = param.surface.initConf(1:3,1:3);
% Find xi
xi = [1 0 0; 0 1 0]*R'*x;
end